### Example Node app in Docker container 

#

#### Create App

```bash
mkdir myfirstapp
cd myfirstapp
```

#

##### create index.js

```js
const express = require('express');
const app = express();

app.get('/', function (req, res) {  
    res.send('Hello World!')  
});

app.listen(3000, function () {  
  console.log('app listening on port 3000!')  
});
```

#

##### create package.json

```
{
    "name": "helloworld",  
    "version": "1.0.0",  
    "description": "Dockerized node.js app",  
    "main": "index.js",  
    "author": "",  
    "license": "ISC",  
    "dependencies": {  
      "express": "^4.16.4"  
    }
 }
```

#

##### create Dockerfile

```docker
FROM node:8  

WORKDIR /app  

COPY package.json /app  

RUN npm install  

COPY . /app  

CMD npm run start
```

___

#### build Image
```
docker build -t vinsproduction/myfirstapp .
```
#

#### run Image
```
docker run -p 8888:3000 --name myfirstapp vinsproduction/myfirstapp
```
#

#### view App

http://localhost:8888

#

#### push Image
```
docker login

docker push vinsproduction/myfirstapp
```
#
