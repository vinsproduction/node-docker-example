const express = require('express');
const PORT = 3000;
const app = express();

app.get('/', function (req, res) {
    res.send('Hello Vins!');
});

app.listen(PORT, function () {
    console.log(`App is running: http://localhost:${PORT}`);
});
